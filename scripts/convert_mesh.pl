use 5.14.0;
use Data::MessagePack;
use Regexp::Common qw/number/;

my $input_filename = @ARGV[0] or die;
my $output_filename = $input_filename;
$output_filename =~ s/\.obj$/\.msh/ or die;

my ($positions, $texcoords, $normals) = (
		[[0.0, 0.0, 0.0]],
		[[0.0, 0.0, 0.0]],
		[[0.0, 0.0]]);
my @vb = ();
my @ib = ();
my %geometry = ();

my %patterns = (
		'v'  => qr/^
			v \s+
			($RE{num}{real}) \s+
			($RE{num}{real}) \s+
			($RE{num}{real}) \s*$
		/x,
		'vt' => qr/^
			vt \s+
			($RE{num}{real}) \s+
			($RE{num}{real}) \s+
			$RE{num}{real} \s*$
		/x,
		'vn' => qr/^
			vn \s+
			($RE{num}{real}) \s+
			($RE{num}{real}) \s+
			($RE{num}{real}) \s*$
		/x,
		'f'  => qr/^
			f \s+
			(.*)$
		/x,
	       );

my %handlers = (
		'v'  => \&add_position,
		'vt' => \&add_texcoord,
		'vn' => \&add_normal,
		'f'  => \&emit_face,
	       );

my ($vs, $vts, $vns) = (0, 0, 0);

while (<>)
{
	s/#.*//;
	next if /^(\s)*$/;
	chomp;

	my $s = $_;

	for my $key (keys %patterns) {
		my @matches = ($s =~ /$patterns{$key}/x);
		if (@matches) {
			my $f = $handlers{$key};
			&$f(@matches);
		}
	}
}

say "v: $vs, vt: $vts, vn: $vns";

my $data = {
	vb => pack('d*', @vb),
	ib => pack('V*', @ib),
	count => scalar @ib,
};

my $mp = Data::MessagePack->new();
my $packed = $mp->pack($data);

say length($packed);
open(my $out, ">:raw", $output_filename);
print $out $packed;
close($out);


sub add_position {
	push @$positions, \@_;
}

sub add_texcoord {
	push @$texcoords, \@_;
}

sub add_normal {
	push @$normals, \@_;
}

sub make_vertex_key {
	my $s = shift;
	my $i = $RE{num}{int};
	my ($v,$t,$n);
	if ($s =~ qr%^ ($i) $%x) {
		$v = $1;
	}
	elsif ($s =~ qr%^ ($i)/($i) $%x) {
		($v,$t) = ($1,$2);
	}
	elsif ($s =~ qr%^ ($i) / ($i) / ($i) $%x) {
		($v,$t,$n) = ($1,$2,$3);
	}
	elsif ($s =~ qr%^ ($i) / / $(i) $%x) {
		($v,$n) = ($1,$2);
	}
	$v = @$positions - $v - 2 if $v < 0;
	$t = @$normals   - $t - 2 if $t < 0;
	$n = @$texcoords - $n - 2 if $n < 0;
	($v,$t,$n,pack('NNN', $v, $t, $n));
}

sub emit_face {
	my $line = shift;
	my @indices;
	for my $tup (split(/\s+/, $line)) {
		my ($v,$t,$n,$k) = &make_vertex_key($tup);
		if (exists($geometry{$k})) {
			my $idx = $geometry{$k};
			push @ib, $idx;
			print 'r';
		}
		else {
			push @vb, @{$positions->[$v]};
			push @vb, @{$texcoords->[$t]};
			push @vb, @{$normals->[$n]};
			my $idx = keys %geometry;
			push @ib, $idx;
			$geometry{$k} = $idx;
			print 'i';
		}
	}
	print ' ';
}

1;
