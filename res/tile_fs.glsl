#version 330

in Geometry
{
	vec3 fs_Normal;
	vec2 fs_Texcoord;
	vec3 fs_Light;
};

out vec4 outputColor;

uniform sampler2D diffuseTexture;

void main()
{
#if 0
	vec3 color = fs_Normal + vec3(1.0, 1.0, 1.0) + vec3(fs_Texcoord, 0.0);
	color /= 2.0;
#else
	vec3 Kd = texture2D(diffuseTexture, fs_Texcoord).rgb;
	float NdotL = clamp(dot(fs_Normal, fs_Light), 0, 1);
	vec3 color = Kd * mix(1.0, NdotL, 0.5);
#endif
	outputColor = vec4(color, 1.0);
}
