#version 330

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

in Vertex
{
	vec4 position;
	vec3 normal;
	vec2 texcoord;
};

out Geometry
{
	vec3 fs_Normal;
	vec2 fs_Texcoord;
	vec3 fs_Light;
};

void main()
{
	gl_Position = projection * view * model * position;
	fs_Normal = mat3(view * model) * normal;
	fs_Texcoord = texcoord;
	fs_Light = mat3(view) * vec3(0,1,0);
}
