cmake_minimum_required(VERSION 2.8)

add_definitions("-std=c11")

include_directories("include")
add_library(STBImage src/stb_image.c)
