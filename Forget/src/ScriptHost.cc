#include <map>
#include <string>
#include "ScriptHost.h"
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/trim.hpp>

#include <json/json.h>
#include <mongoose.h>

static void* RemoteScriptCallback(mg_event event, mg_connection* conn)
{
	mg_request_info* req = mg_get_request_info(conn);
	ScriptHost* self = (ScriptHost*)req->user_data;

	if (event == MG_NEW_REQUEST && strcmp(req->request_method, "POST") == 0 && strcmp(req->uri, "/http") == 0)
	{
		std::vector<uint8_t> acc;
		int n;
		uint8_t buf[1024];
		while ((n = mg_read(conn, buf, sizeof(buf))) > 0)
		{
			acc.insert(acc.end(), buf, buf + n);
		}
		{
			boost::unique_lock<boost::mutex> _(self->pendingMutex);
			self->pendingMessages.push_back(acc);
		}
		mg_printf(conn, "HTTP/1.0 200 OK\r\n"
			"Content-Length: 0\r\n\r\n");
		return "";
	}

	return nullptr;
}

ScriptHost::ScriptHost()
{
}

ScriptHost::~ScriptHost()
{
	for (auto goose : mongeese)
	{
		mg_stop(goose.second);
	}
}

void ScriptHost::RegisterFunction(std::string name, std::function<ScriptHost::HandlerSignature> fun)
{
	handlers[name] = fun;
}

void ScriptHost::Listen(uint16_t port)
{
	assert(mongeese.find(port) == mongeese.end());
	std::string portString = boost::lexical_cast<std::string>(port);
	char const* options[] = {
		"listening_ports", portString.c_str(),
		"document_root", "res/html",
		"index_files", "index.html",
		nullptr };
	auto ctx = mg_start(&RemoteScriptCallback, (void*)(ScriptHost*)this, options);
	mongeese[port] = ctx;
}

void ScriptHost::Update()
{
	Json::Reader r;
	std::deque<std::vector<uint8_t>> messages;
	{
		boost::unique_lock<boost::mutex> _(pendingMutex);
		std::swap(messages, pendingMessages);
	}

	for (auto const& message : messages)
	{
		Json::Value root;
		Json::Value id;
		Json::Value type;
		auto p = (char const*)&message[0];
		if (r.parse(p, p + message.size(), root, false) &&
			(id = root["id"]).isIntegral() &&
			(type = root["type"]).isString())
		{
			auto typeString = type.asString();
			if (typeString == "command")
			{
				Json::Value command = root["command"];
				auto name = command.asString();
				auto I = handlers.find(name);
				if (I != handlers.end())
				{
					I->second(root);
				}
			}
		}
	}
}