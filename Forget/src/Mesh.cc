#include "Mesh.h"
#include "util/Numeric.h"

#include <boost/foreach.hpp>

namespace geom
{
	OBB TransformBoundingVolume(float4x4 const& xform, OBB bv)
	{
		bv.Transform(xform);
		return bv;
	}

	AbstractBinding MakeAbstractBinding(
		std::string semanticName,
		unsigned semanticOrdinal,
		unsigned bufferIndex,
		unsigned componentCount,
		GLenum componentType,
		GLboolean shouldNormalize,
		GLsizei attributeStride,
		unsigned byteOffset)
	{
		AbstractBinding ret;
		CommonBinding common = {
			bufferIndex, componentCount, componentType,
			shouldNormalize, attributeStride, byteOffset };
		(CommonBinding&)ret = common;
		ret.semanticName = semanticName;
		ret.semanticOrdinal = semanticOrdinal;
		return ret;
	}

	boost::shared_ptr<ResolvedMesh> ResolveMesh(AbstractMesh const& source, ShaderLocations const& locs)
	{
		boost::shared_ptr<ResolvedMesh> ret(new ResolvedMesh);
		
		size_t numVBs = source.vertexBuffers.size();
		std::vector<GLuint> rawVBs(numVBs);

		assert(numVBs);
		glGenBuffers(numVBs, &rawVBs[0]);

		for (size_t i = 0; i < numVBs; ++i)
		{
			auto vb = rawVBs[i];
			auto const& src = source.vertexBuffers[i];
			glBindBuffer(GL_ARRAY_BUFFER, vb);
			glBufferData(GL_ARRAY_BUFFER, src.size, src.data.get(), GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			ret->vbs.push_back(boost::shared_ptr<GLuint>(new GLuint(vb), &DeleteGLBuffer));
		}

		GLuint ib;
		glGenBuffers(1, &ib);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, source.indexBuffer.size, source.indexBuffer.data.get(), GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		ret->ib.reset(new GLuint(ib), &DeleteGLBuffer);

		BOOST_FOREACH(auto const& binding, source.bindings)
		{
			ResolvedBinding b;
			(CommonBinding&)b = binding;
			auto sem = MakeSemantic(binding.semanticName, binding.semanticOrdinal);
			auto I = locs.mapping.find(sem);
			if (I == locs.mapping.end())
				continue;
			b.attributeIndex = I->second;
			ret->bindings.insert(b);

			if (binding.semanticName == "POSITION")
			{
				auto posBuffer = source.vertexBuffers[binding.bufferIndex];
				auto fp = posBuffer.data.get() + binding.byteOffset;
				int n = posBuffer.size / binding.attributeStride;
				if (n == 0)
					continue;

				float3 const* p = (float3 const*)(fp);
				float3 min = *p, max = *p;
				for (int i = 1; i < n; ++i)
				{
					min = math::Min(min, *p);
					max = math::Max(max, *p);
					p = (float3 const*)(fp + i*binding.attributeStride);
				}

				OBB obb(AABB(min, max));
				ret->boundingVolume = obb;
			}
		}

		ret->objects = source.objects;

		return ret;
	}

	ShaderLocations::Semantic MakeSemantic(std::string name, unsigned ordinal)
	{
		ShaderLocations::Semantic sem = { name, ordinal };
		return sem;
	}
}