#include "util/Numeric.h"
#include "util/OpenGL.h"
#include <GL/glfw3.h>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional.hpp>
#include <boost/regex.hpp>

#include <cassert>
#include <deque>
#include <fstream>
#include <map>
#include <queue>
#include <set>
#include <vector>

#include "util/DebugHelp.h"
#include "util/Resources.h"
#include "TransformAnimation.h"
#include "Mesh.h"
#include "Shader.h"
#include "Texture.h"
#include "ScriptHost.h"

bool g_running;
int screenWidth = 1280, screenHeight = 720;
bool wantEntityPositionDump = false;
bool wantEntityTransformedDump = false;
enum Movable { EyeMovable, AtMovable };
enum Direction { Left, Right, Down, Up, In, Out };
std::map<Movable, std::map<Direction, bool>> wantMoves;
std::map<Movable, std::map<Direction, float>> moveFades;
bool showAtWidget = true;

float const FadeSpeed = 2.0f;

void AdjustFades(double dt)
{
	for (auto const& move : wantMoves)
	{
		for (auto const& want : move.second)
		{
			auto& f = moveFades[move.first][want.first];
			float dir = (float)(want.second ? 1.0f : -1.0f);
			float amount = dir * FadeSpeed * (float)dt;
			f = math::Clamp(f + amount, 0.0f, 1.0f);
		}
	}
}

void UnloadGameAssets();

int WindowCloseHandler(GLFWwindow wnd)
{
	UnloadGameAssets();
	g_running = false;
	return GL_TRUE;
}

void KeyHandler(GLFWwindow wnd, int key, int pressed)
{
	if (key == GLFW_KEY_ESCAPE)
	{
		UnloadGameAssets();
		g_running = false;
	}
	if (key == GLFW_KEY_SPACE)
	{
		wantEntityPositionDump = true;
		wantEntityTransformedDump = true;
	}
	if (key == GLFW_KEY_W)
		wantMoves[EyeMovable][In] = !!pressed;
	if (key == GLFW_KEY_S)
		wantMoves[EyeMovable][Out] = !!pressed;
	if (key == GLFW_KEY_A)
		wantMoves[EyeMovable][Left] = !!pressed;
	if (key == GLFW_KEY_D)
		wantMoves[EyeMovable][Right] = !!pressed;
	if (key == GLFW_KEY_UP)
		wantMoves[AtMovable][In] = !!pressed;
	if (key == GLFW_KEY_DOWN)
		wantMoves[AtMovable][Out] = !!pressed;
	if (key == GLFW_KEY_LEFT)
		wantMoves[AtMovable][Left] = !!pressed;
	if (key == GLFW_KEY_RIGHT)
		wantMoves[AtMovable][Right] = !!pressed;
	if (key == GLFW_KEY_PAGE_UP)
		wantMoves[AtMovable][Up] = !!pressed;
	if (key == GLFW_KEY_PAGE_DOWN)
		wantMoves[AtMovable][Down] = !!pressed;
	if (key == GLFW_KEY_X && !pressed)
		showAtWidget = !showAtWidget;
}

typedef unsigned EntityID;
typedef unsigned AnimationID;
typedef std::string MeshName;

EntityID entitySupply;
AnimationID animationSupply;

struct BoundingBundle
{
	OBB local;
	OBB world;
};

struct AnimationState
{
	int startTick;
	boost::shared_ptr<TransformAnimation> anim;
};

struct Entities
{
	std::set<EntityID> ids;
	std::map<EntityID, BoundingBundle> bounds;
	std::map<EntityID, float4x4> worlds;
	std::map<EntityID, float4x4> transforms;
	std::map<EntityID, AnimationState> animations;
	std::map<EntityID, MeshName> meshNames;

	std::map<EntityID, std::map<std::string, std::string>> attributes;
};

struct Animations
{
	std::set<AnimationID> ids;
};

Entities entities;
Animations animations;
std::map<std::string, boost::shared_ptr<geom::ResolvedMesh>> meshes;

GLuint tileProgram = 0;
geom::ShaderLocations tileUniformLocs;

std::map<std::string, GLuint> textures;
GLuint clampBilinearSampler;

float4x4 viewMatrix;
float4x4 projectionMatrix;

float4x4 atTransform;

int lastMouseX, lastMouseY;
std::deque<float2> pendingClicks;

void MouseMoveHandler(GLFWwindow wnd, int x, int y)
{
	lastMouseX = x;
	lastMouseY = y;
}

void MouseButtonHandler(GLFWwindow wnd, int button, int isActive)
{
	if (! isActive)
	{
		pendingClicks.push_back(float2((float)lastMouseX, (float)lastMouseY));
	}
}

struct ProgramInterfaceResults
{
	GLint activeResources;
	GLint maxNameLength;
	GLint maxNumActiveVariables;
	GLint maxNumCompatibleSubroutines;
};

ProgramInterfaceResults GetProgramInterface(GLuint program, GLenum iface)
{
	ProgramInterfaceResults res = {};
	glGetProgramInterfaceiv(program, iface, GL_ACTIVE_RESOURCES, &res.activeResources);
	glGetProgramInterfaceiv(program, iface, GL_MAX_NAME_LENGTH, &res.maxNameLength);
	glGetProgramInterfaceiv(program, iface, GL_MAX_NUM_ACTIVE_VARIABLES, &res.maxNumActiveVariables);
	glGetProgramInterfaceiv(program, iface, GL_MAX_NUM_COMPATIBLE_SUBROUTINES, &res.maxNumCompatibleSubroutines);
	return res;
}

void LoadGameAssets()
{
	assert(!tileProgram);
	auto tileVS = ShaderFromResource(GL_VERTEX_SHADER, "tile_vs.glsl");
	auto tileFS = ShaderFromResource(GL_FRAGMENT_SHADER, "tile_fs.glsl");

	tileProgram = glCreateProgram();
	glAttachShader(tileProgram, tileVS.shader);
	glAttachShader(tileProgram, tileFS.shader);
	glLinkProgram(tileProgram);

	GLint status;
	glGetProgramiv(tileProgram, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		GLint infoLogLength = 0;
		glGetProgramiv(tileProgram, GL_INFO_LOG_LENGTH, &infoLogLength);
		std::vector<char> infoLog(infoLogLength);
		glGetProgramInfoLog(tileProgram, infoLog.size(), nullptr, &infoLog[0]);
		OutputDebugStringA(&infoLog[0]);
		OutputDebugStringA("\n");
	}

	glDeleteShader(tileVS.shader);
	glDeleteShader(tileFS.shader);

	{
		geom::ShaderLocations uniformLocs;
	
		char const* names[] = { "model", "view", "projection", "diffuseTexture" };
		char const* semantics[] = { "MODEL", "VIEW", "PROJECTION", "DIFFUSE" };

		for (int i = 0; i < 4; ++i)
		{
			GLuint loc = glGetUniformLocation(tileProgram, names[i]);
			if (loc != (GLuint)-1)
				uniformLocs.mapping[geom::MakeSemantic(semantics[i])] = loc;
		}

		::tileUniformLocs = uniformLocs;
	}


	{
		geom::ShaderLocations locs;
		geom::ShaderLocations::Semantic sem = {};
		sem.name = "POSITION";
		locs.mapping[sem] = 0;
		sem.name = "NORMAL";
		locs.mapping[sem] = 1;
		sem.name = "TEXCOORD";
		locs.mapping[sem] = 2;
		
		boost::shared_ptr<geom::AbstractMesh> rawTile = geom::LoadObj("res/axis_widget.obj");
		meshes["tile"] = geom::ResolveMesh(*rawTile, locs);
		for (int i = 0; i <= 20; ++i)
		{
			std::string name = "digit" + boost::lexical_cast<std::string>(i);
			std::string s = "res/" + name + ".obj";
			boost::shared_ptr<geom::AbstractMesh> raw = geom::LoadObj(s);
			meshes[name] = geom::ResolveMesh(*raw, locs);
		}
	}

	textures["X"] = LoadTexture("res/red.png", GL_UNSIGNED_BYTE);
	textures["Y"] = LoadTexture("res/green.png", GL_UNSIGNED_BYTE);
	textures["Z"] = LoadTexture("res/blue.png", GL_UNSIGNED_BYTE);
	textures["wire_228153184"] = LoadTexture("res/062.png", GL_UNSIGNED_BYTE);
	textures["Tile"] = LoadTexture("res/078.png", GL_UNSIGNED_BYTE);
	textures["Image"] = LoadTexture("res/138.jpg", GL_UNSIGNED_BYTE);
	{
		float maxAF = 1.0f;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAF);
		glGenSamplers(1, &clampBilinearSampler);
		glSamplerParameteri(clampBilinearSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(clampBilinearSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(clampBilinearSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(clampBilinearSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glSamplerParameterf(clampBilinearSampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAF);
	}
}

void UnloadGameAssets()
{
	BOOST_FOREACH(auto const& pair, textures)
	{
		glDeleteTextures(1, &pair.second);
	}
	meshes.clear();
	glDeleteProgram(tileProgram);
	tileProgram = 0;
}

template <typename Binding>
void BindStream(GLuint vb, Binding const& binding)
{
	glBindBuffer(GL_ARRAY_BUFFER, vb);
	glEnableVertexAttribArray(binding.attributeIndex);
	GLboolean normalized = binding.shouldNormalize ? GL_TRUE : GL_FALSE;
	glVertexAttribPointer(binding.attributeIndex, binding.componentCount,
		binding.componentType, normalized, binding.attributeStride,
		(GLvoid const*)(uintptr_t)binding.byteOffset);
}

template <typename BufferVector, typename BindingSet>
void BindStreams(BufferVector const& vbs, BindingSet const& bindings)
{
	BOOST_FOREACH(auto const& binding, bindings)
	{
		BindStream(*vbs[binding.bufferIndex], binding);
	}
}

template <typename BindingSet>
void UnbindStreams(BindingSet const& bindings)
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	std::set<GLuint> arrays;
	BOOST_FOREACH(auto const& binding, bindings)
	{
		arrays.insert(binding.attributeIndex);
	}

	BOOST_FOREACH(auto index, arrays)
	{
		glDisableVertexAttribArray(index);
	}
}

float4 clickLine[2] = {};

void DrawGame()
{
	double const t = glfwGetTime();
	glUseProgram(tileProgram);

	geom::ShaderLocations::Semantic semantics[] = {
		geom::MakeSemantic("MODEL"),
		geom::MakeSemantic("VIEW"),
		geom::MakeSemantic("PROJECTION"),
		geom::MakeSemantic("DIFFUSE"),
		geom::MakeSemantic("SELECTED"),
	};

	glUniformMatrix4fv(tileUniformLocs.mapping[semantics[1]], 1, GL_TRUE, viewMatrix.ptr());
	glUniformMatrix4fv(tileUniformLocs.mapping[semantics[2]], 1, GL_TRUE, projectionMatrix.ptr());

	std::deque<std::pair<float4x4, boost::shared_ptr<geom::ResolvedMesh>>> drawables;
	BOOST_FOREACH(auto xfp, entities.transforms)
	{
		auto id = xfp.first;
		auto xform = xfp.second;
		auto meshI = meshes.find(entities.meshNames[xfp.first]);
		if (meshI == meshes.end())
			continue;
		auto mesh = meshI->second;
		drawables.push_back(std::make_pair(xform, mesh));
	}

	// make at drawable
	if (showAtWidget)
	{
		auto xform = atTransform;
		auto mesh = meshes["tile"];
		drawables.push_back(std::make_pair(xform, mesh));
	}

	std::string s;
	boost::shared_ptr<geom::ResolvedMesh> currentMesh;
	BOOST_FOREACH(auto xfp, drawables)
	{
		float4x4 tileWorld = xfp.first;
		if (wantEntityTransformedDump)
		{
			float4x4 m = (projectionMatrix * viewMatrix * tileWorld);
			float3 p = m.TransformPos(0.0f, 0.0f, 0.0f);
			float3 x = m.TransformPos(1.0f, 0.0f, 0.0f);
			float3 y = m.TransformPos(0.0f, 1.0f, 0.0f);
			float3 z = m.TransformPos(0.0f, 0.0f, 1.0f);
			char line[1000];
			sprintf(line, "%f %f %f\n%f %f %f\n%f %f %f\n%f %f %f\n--\n",
				p.x, p.y, p.z,
				x.x, x.y, x.z,
				y.x, y.y, y.z,
				z.x, z.y, z.z);
			s += line;
		}

		auto mesh = xfp.second;
		if (mesh != currentMesh)
		{
			BindStreams(mesh->vbs, mesh->bindings);
			currentMesh = mesh;
		}
		glUniformMatrix4fv(tileUniformLocs.mapping[semantics[0]], 1, GL_TRUE, tileWorld.ptr());
		glUniform1ui(tileUniformLocs.mapping[semantics[3]], 0);
		glBindSampler(0, clampBilinearSampler);

		BOOST_FOREACH(geom::Object obj, mesh->objects)
		{
			glActiveTexture(GL_TEXTURE0 + 0);
			glBindTexture(GL_TEXTURE_2D, textures[obj.material]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *mesh->ib);
			glDrawElements(GL_TRIANGLES, obj.primitiveCount*3, GL_UNSIGNED_INT, (GLvoid const*)(obj.offset*4));
		}
	}
	if (currentMesh)
		UnbindStreams(currentMesh->bindings);

	glUseProgram(0);
	if (wantEntityTransformedDump)
	{
		MessageBoxA(nullptr, s.c_str(), "Entities transformed", MB_OK);
		wantEntityTransformedDump = false;
	}
}

void ScreenToWorldLineSegment(float2 point, float4* nearOut, float4* farOut)
{
	// TODO: Verify matrix math.
	float4x4 screenToNDC =
		float4x4::Translate(-1.0f, 1.0f, 0.0f) *
		float4x4::Scale(2.0f/screenWidth, -2.0f/screenHeight, 1.0f);

	float4x4 inv = (projectionMatrix*viewMatrix).Inverted();
	float4 screenCoord(point, 0.0, 1.0);
	float4 ndcCoord = screenToNDC * screenCoord;
	float2 ndcXY = ndcCoord.xy();

	float4 nearPoint = inv * float4(ndcXY.x, ndcXY.y, -1.0f, 1.0f);
	float4 farPoint  = inv * float4(ndcXY.x, ndcXY.y,  1.0f, 1.0f);
		
	nearPoint /= nearPoint.w;
	farPoint /= farPoint.w;
	
	*nearOut = nearPoint;
	*farOut = farPoint;
}

int gameTicks = 0;
double rootTime;
float3 const defaultEye = 5.0f * float3(0.0f, 10.0f, 50.0f).Normalized();
float3 eye = defaultEye;

float3 const defaultAt = float3(0, 0, 0);
float3 at = defaultAt;

Quat const defaultEyeOrientation = Quat(0, 0, 0, 1);
Quat eyeOrientation = defaultEyeOrientation;

float const defaultEyeBase = 5.0f;
float eyeBase = 5.0f;

void TickLogic()
{
	float3 const Y = float3::unitY;
	float3 const Z = float3::unitZ;

	double const dt = 1.0/120.0;
	double t = glfwGetTime();
	while (t - rootTime > dt * gameTicks)
	{
		AdjustFades(dt);

		{
			float const speed = 2.0f;
			float const leftAngle = -(float)(moveFades[EyeMovable][Left] * speed * dt);
			float const rightAngle = (float)(moveFades[EyeMovable][Right] * speed * dt);
			Quat const rotation = Quat::RotateY(leftAngle + rightAngle);
			eyeOrientation = eyeOrientation * rotation;
		}

		{
			float const speed = 2.0f;
			float const inAmount = -(float)(moveFades[EyeMovable][In] * speed * dt);
			float const outAmount = (float)(moveFades[EyeMovable][Out] * speed * dt);
			eyeBase = (std::max)(0.0f, eyeBase + inAmount + outAmount);
		}

		{
			float const speed = 2.0f;
			float const leftAmount  = -(float)(moveFades[AtMovable][Left] * speed * dt);
			float const rightAmount =  (float)(moveFades[AtMovable][Right] * speed * dt);
			float const downAmount  = -(float)(moveFades[AtMovable][Down] * speed * dt);
			float const upAmount    =  (float)(moveFades[AtMovable][Up] * speed * dt);
			float const inAmount    = -(float)(moveFades[AtMovable][In] * speed * dt);
			float const outAmount   =  (float)(moveFades[AtMovable][Out] * speed * dt);
			at += float3(leftAmount + rightAmount, downAmount + upAmount, inAmount + outAmount);
		}

		atTransform = (float4x4)float4x4::Translate(at);

		gameTicks++;
		projectionMatrix = MakePerspective(90.0f, 1.0f, 5000.0f, (float)screenWidth, (float)screenHeight);
		float4x4 cameraMatrix =
			float4x4::LookAt(eye, at, -float3::unitZ, float3::unitY, float3::unitY);
		viewMatrix = cameraMatrix.Inverted();
		
		float const eyeDistance = 1.0f + pow(eyeBase, 2.2f);
		viewMatrix = float4x4::Translate(0.0f, 0.0f, -eyeDistance) *
			eyeOrientation.Inverted().ToFloat4x4() *
			float4x4::Translate(-at);

#if 0
		if (pendingClicks.size())
		{
			glm::vec4 nearPoint, farPoint;
			ScreenToWorldLineSegment(pendingClicks.front(), &nearPoint, &farPoint);
			pendingClicks.pop_front();

			clickLine[0] = nearPoint;
			clickLine[1] = farPoint;
					
			boost::optional<float> intersectT;
			boost::optional<TileID> intersectID;
			BOOST_FOREACH(auto const& bvp, tiles.bounds)
			{
				auto bv = bvp.second;
				auto res = geom::FindLine(glm::vec3(nearPoint), glm::vec3(farPoint), bv.world);
				if (! res.intersect) continue;

				if (! intersectT || *intersectT > res.t)
				{
					intersectT = res.t;
					intersectID = bvp.first;
							
				}
			}

			bool newMatch = intersectID; // TODO: fixme
			if (newMatch)
			{
				TileID id = *intersectID;

				double animationDuration = 0.3;
				int tickDuration = (int)(animationDuration/dt);

				glm::vec2 axisDirection = tiles.flipAxes[id];
				glm::vec3 rotationAxis(axisDirection.x, 0.0f, axisDirection.y);

				tileAnimations[id].anim.reset(new BounceFlipAnimation((float)animationDuration, 1.0f /*distance*/, Y /*along-axis*/, 0.5f /*rotations*/, rotationAxis /*around-axis*/));
				tileAnimations[id].startTick = gameTicks;

				if (ph.hasEarlierTile)
					gamePhase.openingTile = PhaseData<OpeningTile>::SecondTile(ph.earlierTile, id, tickDuration);
				else
					gamePhase.openingTile = PhaseData<OpeningTile>::FirstTile(id, tickDuration);
			}
		}
#endif

		std::map<EntityID, float4x4> localTransforms;
		BOOST_FOREACH(auto& a, entities.animations)
		{
			float t = (float)(dt * (gameTicks - a.second.startTick));
			localTransforms[a.first] = a.second.anim->Compute(t);
		}

		std::string s;
		for (auto I = entities.worlds.begin(); I != entities.worlds.end(); ++I)
		{
			// TODO: Verify matrix math.
			auto id = I->first;
			float4x4 const& shiftMatrix = I->second;
			auto J = localTransforms.find(id);
			float4x4 local = float4x4::identity;
			if (J != localTransforms.end())
				local = J->second;
			float4x4 xform = entities.transforms[id] = shiftMatrix * local;
			entities.bounds[id].world = geom::TransformBoundingVolume(xform, entities.bounds[id].local);
			if (wantEntityPositionDump)
			{
				float3 p = xform.TransformPos(0.0f, 0.0f, 0.0f);
				char line[100];
				sprintf(line, "%f %f %f\n", p.x, p.y, p.z);
				s += line;
			}
		}
		if (wantEntityPositionDump)
		{
			MessageBoxA(nullptr, s.c_str(), "Entity positions", MB_OK);
			wantEntityPositionDump = false;
		}
	}
}

void UpdateVisuals()
{
}

int AddEntity(std::string meshName, float4x4 const* world)
{
	auto id = entitySupply++;
	entities.meshNames[id] = meshName;
	entities.bounds[id].local = meshes[meshName]->boundingVolume;
	if (world)
		entities.worlds[id] = *world;
	entities.transforms[id] = float4x4::identity;
	entities.ids.insert(id);
	return id;
}

static int boneId = 0;

void AddEntityCommand(Json::Value o)
{
	std::string name = "digit" + boost::lexical_cast<std::string>(boneId);
	boneId = (boneId+1)%20;
	float4x4 world = float4x4::identity;
	Json::Value mesh = o.get("mesh", Json::Value(name));
	Json::Value xform = o.get("xform", Json::Value());
	if (xform.isArray() && xform.size() >= 12)
	{
		auto n = xform.size();
		for (size_t i = 0; i < n; ++i)
		{
			world.ptr()[i] = xform[i].asFloat();
		}
	}
	AddEntity(mesh.asString(), &world);
}

void SetFloat3Command(float3* vp, Json::Value o)
{
	Json::Value pos = o["pos"];
	if (pos.size() != 3)
	{
		OutputDebugStringA("pos did not have three components.\n");
		return;
	}
	float3 v(pos[0].asFloat(), pos[1].asFloat(), pos[2].asFloat());
	*vp = v;
}

void ShiftFloat3Command(float3* vp, Json::Value o)
{
	Json::Value rel = o["rel"];
	if (rel.size() != 3)
	{
		OutputDebugStringA("pos did not have three components.\n");
		return;
	}
	float3 v(rel[0].asFloat(), rel[1].asFloat(), rel[2].asFloat());
	*vp = *vp + v;
}

void MoveEntity(EntityID id, float4x4 world)
{
	if (entities.ids.find(id) == entities.ids.end())
		return;
	entities.worlds[id] = world;
}

int Main()
{
	glfwInit();
	{
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		glfwWindowHint(GLFW_FSAA_SAMPLES, 4);
		GLFWwindow wnd = glfwCreateWindow(screenWidth, screenHeight, GLFW_WINDOWED, "Forget", nullptr);
		glfwSetWindowCloseCallback(wnd, &WindowCloseHandler);
		glfwSetKeyCallback(wnd, &KeyHandler);
		glfwSetMouseButtonCallback(wnd, &MouseButtonHandler);
		glfwSetCursorPosCallback(wnd, &MouseMoveHandler);
		glfwMakeContextCurrent(wnd);
		gl3wInit();

		LoadGameAssets();
		entitySupply = 0;

		float3 const rowSpacing = float3(1.5, 1.5, 1.5);
		int const layers = 2;
		int const rows = 2;
		int const cols = 2;
		for (int layer = 0; layer < layers; ++layer)
		{
			for (int row = 0; row < rows; ++row)
			{
				for (int col = 0; col < cols; ++col)
				{
					MeshName const meshName("tile");
					float3 pos = float3(col - (cols-1)/2.0f, row - (rows-1)/2.0f, layer - (layers-1)/2.0f).Mul(rowSpacing);
					float4x4 world = float4x4::Translate(pos);
					AddEntity(meshName, &world);
				}
			}
		}

		auto _1 = std::placeholders::_1;
		auto clearEntities = [](Json::Value o) { entities = Entities(); AddEntity("tile", nullptr); boneId = 0; };
		clearEntities(Json::Value());
		ScriptHost scriptHost;
		scriptHost.RegisterFunction("ClearEntities", clearEntities);
		scriptHost.RegisterFunction("AddEntity", &AddEntityCommand);
		scriptHost.RegisterFunction("SetAt", std::bind(&SetFloat3Command, &at, _1));
		scriptHost.RegisterFunction("SetEye", std::bind(&SetFloat3Command, &eye, _1));
		scriptHost.RegisterFunction("ShiftAt", std::bind(&ShiftFloat3Command, &at, _1));
		scriptHost.RegisterFunction("ShiftEye", std::bind(&ShiftFloat3Command, &eye, _1));
		scriptHost.Listen(9090);
		
		glEnable(GL_DEPTH_TEST);

		g_running = true;
		glClearColor(0.8f, 0.8f, 0.8f, 1.0f);
		glClearDepth(1.0);
		while (g_running)
		{
			scriptHost.Update();
			TickLogic();
			UpdateVisuals();
			
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			DrawGame();
			glfwPollEvents();
			glfwSwapBuffers(wnd);
		}
		glfwMakeContextCurrent(0);
		glfwDestroyWindow(wnd);
	}
	glfwTerminate();
	return 0;
}

#if defined(WIN32)
#include <Windows.h>
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	return Main();
}
#else
int main()
{
	return Main();
}
#endif
