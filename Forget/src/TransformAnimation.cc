#include "TransformAnimation.h"

BounceFlipAnimation::BounceFlipAnimation(float duration, float distance, float3 alongAxis, float rotations, float3 aroundAxis, float angleOffset)
	: duration(duration)
	, distance(distance)
	, alongAxis(alongAxis.Normalized())
	, rotations(rotations)
	, aroundAxis(aroundAxis.Normalized())
	, angleOffset(angleOffset)
{}

float4x4 BounceFlipAnimation::Compute(double t) const
{
	// TODO: Verify matrix math.
	float a = (float)(t / duration);
	a = (std::min)(a, 1.0f);
	float4x4 tileRotation = float4x4::RotateAxisAngle(aroundAxis, angleOffset + 2*math::pi*rotations);
	float4x4 tileDisplacement = float4x4::Translate(0.0f, distance*fabs(sin(a*pi)), 0.0f);
	return tileDisplacement * tileRotation;
}