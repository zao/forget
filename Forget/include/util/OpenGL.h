#pragma once

#define GLCOREARB_PROTOTYPES 1
#include <GL/gl3w.h>

inline void DeleteGLBuffer(GLuint* p)
{
	glDeleteBuffers(1, p);
	delete p;
}

enum {
	GL_TEXTURE_MAX_ANISOTROPY_EXT     = 0x84FE,
	GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT = 0x84FF,
};