#pragma once
#include <string>
#include <vector>

#if defined(WIN32)
inline std::wstring WidenString(std::string str)
{
	if (str.empty())
		return L"";

	auto cch = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.size(), 0, 0);
	std::vector<wchar_t> buf(cch);
	MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.size(), &buf[0], buf.size());
	return std::wstring(&buf[0], &buf[0] + cch);
}
#endif

inline std::vector<char> LoadEmbeddedResource(std::string name)
{
#if defined(WIN32)
	HMODULE mod = GetModuleHandleW(nullptr);
	HRSRC res = FindResourceW(mod, WidenString(name).c_str(), RT_RCDATA);
	HGLOBAL g = LoadResource(mod, res);
	char const* bytes = (char const*)LockResource(g);
	DWORD count = SizeofResource(mod, res);
	std::vector<char> ret(bytes, bytes + count);
	return ret;
#else
	throw std::runtime_error("Embedded resource loading not supported.");
#endif
}
