#pragma once

#include <cstdint>
#include <MathGeoLib.h>

namespace math
{
	inline float Random01()
	{
		auto r = rand();
		return r/(float)(RAND_MAX);
	}

	inline float Pi()
	{
		return math::pi;
	}

	inline float2 RandomDirection2D()
	{
	  float azimuth = Random01() * 2 * Pi();
	  return float2(cos(azimuth), sin(azimuth));
	}

	inline float3 RandomDirection3D()
	{
	  float z = (2*Random01()) - 1; // z is in the range [-1,1]
	  float2 planar = RandomDirection2D() * sqrtf(1-z*z);
	  return float3(planar.x, planar.y, z);
	}

	inline float4x4 MakePerspective(float fovY, float zNear, float zFar, float width, float height)
	{
		float const aspect = width / height;
		float const range = tan(math::DegToRad(45.0f / 2.0f)) * zNear;
		float h = 2*range * aspect;
		float v = 2*range;
		return float4x4::OpenGLPerspProjRH(zNear, zFar, h, v);
	}
}