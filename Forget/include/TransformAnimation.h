#pragma once
#include "util/Numeric.h"

class TransformAnimation
{
public:
	virtual ~TransformAnimation() {}
	virtual float4x4 Compute(double t) const = 0;
};

class BounceFlipAnimation : public TransformAnimation
{
public:
	BounceFlipAnimation(float duration, float distance, float3 alongAxis, float rotations, float3 aroundAxis, float angleOffset = 0.0f);
	
	virtual float4x4 Compute(double t) const override;

private:
	float duration;
	float distance;
	float3 alongAxis;
	float rotations;
	float3 aroundAxis;
	float angleOffset;
};
