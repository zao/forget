#pragma once
#include <mongoose.h>
#include <cstdint>
#include <deque>
#include <string>
#include <unordered_map>
#include "util/SmartPointer.h"
#include <json/json.h>
#include <boost/thread.hpp>

struct ScriptHost
{
	ScriptHost();
	~ScriptHost();

	typedef void HandlerSignature(Json::Value const& command);
	void RegisterFunction(std::string name, std::function<HandlerSignature> fun);
	void Listen(uint16_t port);
	void Update();

private:
	friend void* RemoteScriptCallback(mg_event, mg_connection*);
	std::map<uint16_t, mg_context*> mongeese;

	std::unordered_map<std::string, std::function<HandlerSignature>> handlers;
	boost::mutex pendingMutex;
	std::deque<std::vector<uint8_t>> pendingMessages;
};